﻿using UnityEngine;

public class TimeMachinePart : MonoBehaviour
{
    TimeMachine timeMachineScript;

    // Start is called before the first frame update
    void Start()
    {
        // Finds the time machine game object in the scene
        timeMachineScript = GameObject.Find("TimeMachine").GetComponent<TimeMachine>();
    }

    // When a game object enters the trigger
    private void OnTriggerEnter(Collider collider)
    {
        // If the collided game object has the player tag
        if (collider.tag == "Player")
        {
            // Adds one to the number of time machine parts
            timeMachineScript.numberOfParts += 1;

            // Destroys the game object
            Destroy(gameObject);
        }
    }
}
