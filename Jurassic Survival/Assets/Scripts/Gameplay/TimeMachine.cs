﻿using UnityEngine;
using UnityEngine.UI;

public class TimeMachine : MonoBehaviour
{
    // Makes a instance for other scripts to access
    public static TimeMachine instance { set; get; }

    [HideInInspector]
    public int numberOfParts = 0;
    public Text objectiveText;
    public Text interactText;
    public GameObject builtTimeMachine;
    public GameObject notBuiltTimeMachine;

    int maxNumberOfParts;

    // Start is called before the first frame update
    void Start()
    {
        // 
        foreach (var gameObj in GameObject.FindGameObjectsWithTag("TimeMachinePart"))
        {
            maxNumberOfParts++;
        }

        builtTimeMachine.SetActive(false);

        // Sets the text to nothing
        interactText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (numberOfParts != maxNumberOfParts)
        {
            objectiveText.text = "Collect Time Machine Parts: " + numberOfParts + "/" + maxNumberOfParts;
        } else
        {
            objectiveText.text = "Go to the time machine and escape the island";
            builtTimeMachine.SetActive(true);
            notBuiltTimeMachine.SetActive(false);
        }
    }

    // When a game object enters the trigger
    private void OnTriggerEnter(Collider collider)
    {
        // If the collided game object has the player tag
        if (collider.tag == "Player")
        {
            // If the number of time machine part
            if (numberOfParts == maxNumberOfParts)
            {
                GameManager.instance.WinGame();
            }
            else
            {
                interactText.text = "You need " + maxNumberOfParts + " Time Machine Part to build the time machine.";
            }
        }
    }

    // When a game object leaves the trigger
    private void OnTriggerExit(Collider collider)
    {
        // Sets the text to nothing
        interactText.text = "";
    }
}