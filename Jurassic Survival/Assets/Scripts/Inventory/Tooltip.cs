﻿using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour 
{
    private Text tooltipText;

    // Start is called before the first frame update
    void Start () {
        // Finds the text component on the child game objects
        tooltipText = GetComponentInChildren<Text>();

        // Hides the item tooltip
        gameObject.SetActive(false);
	}

    // Generate the tooltip text
    public void GenerateTooltip(Item item)
    {
        // Removes any text from the statText variable
        string statText = "";

        // If the items has more than 0 stats
        if (item.stats.Count > 0)
        {
            // For each item stat in the current item stats
            foreach(var stat in item.stats)
            {
                statText += stat.Key.ToString() + ": " + stat.Value + "\n";
            }
        }
        
        // Formats the tooltip string to display to correctly
        string tooltip = string.Format("<b>{0}</b>\n{1}\n\n<b>{2}</b>", item.itemName, item.description, statText);

        // Assigns the tooltip string to the tooltip text
        tooltipText.text = tooltip;

        // Shows the item tooltip
        gameObject.SetActive(true);
    }
}
