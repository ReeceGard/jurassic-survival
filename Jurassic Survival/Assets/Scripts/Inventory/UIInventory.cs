﻿using System.Collections.Generic;
using UnityEngine;

public class UIInventory : MonoBehaviour 
{
    public List<UIItem> uiItems = new List<UIItem>();
    public GameObject slotPrefab;
    public Transform slotPanel;
    public int numberOfSlots = 16;

    private void Awake()
    {
        BuildInventoryUI();
    }

    public void BuildInventoryUI()
    {
        // Loops through all of the number of slots
        for (int i = 0; i < numberOfSlots; i++)
        {
            // Creates an instance of the slot prefab
            GameObject instance = Instantiate(slotPrefab);
            // Sets the parent of the slot to the slot panel
            instance.transform.SetParent(slotPanel);

            uiItems.Add(instance.GetComponentInChildren<UIItem>());
        }
    }

    public void UpdateSlot(int slot, Item item)
    {
        uiItems[slot].UpdateItem(item);
    }

    public void AddNewItem(Item item)
    {
        UpdateSlot(uiItems.FindIndex(i=> i.item == null), item);
    }

    public void RemoveItem(Item item)
    {
        UpdateSlot(uiItems.FindIndex(i=> i.item == item), null);
    }
}
