﻿using UnityEngine;

public class ItemFollowMouse : MonoBehaviour
{
	// Update is called once per frame
	void LateUpdate()
	{
		// Set the position of the game object to mouse position 
		transform.position = Input.mousePosition;
	}
}
