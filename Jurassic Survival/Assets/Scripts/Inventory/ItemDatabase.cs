﻿using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    public List<Item> items = new List<Item>();

    // Awake is called when the object is initialised
    private void Awake()
    {
        BuildDatabase();
    }

    public Item GetItem(int id)
    {
        return items.Find(item => item.id == id);
    }

    public Item GetItem(string itemName)
    {
        return items.Find(item => item.itemName == itemName);
    }

    void BuildDatabase()
    {
        items = new List<Item>() {
                new Item(0, "Health Pack", "The health pack will give the player more HP.", false, true,
                new Dictionary<string, int>
                {
                    {"Health", 25}
                }),

                new Item(1, "Blueberries", "Blueberries can be eaten.", false, true,
                new Dictionary<string, int>
                {
                    {"Hunger", 10}
                }),

                new Item(2, "Dinosaur Meat", "Meat that comes from a dinosaur.", false, true,
                new Dictionary<string, int>
                {
                    {"Hunger", 25}
                })
            };
    }
}
