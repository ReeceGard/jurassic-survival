﻿using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    public string itemName;

    Inventory inventory;

    // Start is called before the first frame update
    void Start()
    {
        inventory = GameObject.Find("FirstPersonPlayer").GetComponent<Inventory>();
    }

    public void PickupItem()
    {
        inventory.GiveItem(itemName);
        Destroy(gameObject);
    }
}
