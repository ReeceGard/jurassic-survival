﻿using System.Collections.Generic;
using UnityEngine;

public class Item
{
    public int id;
    public string itemName;
    public string description;
    public bool canEquip;
    public bool canUse;
    public Sprite icon;
    public GameObject itemPickup;
    public Dictionary<string, int> stats = new Dictionary<string, int>();
    public Item(int id, string itemName, string description, bool canEquip, bool canUse,
        Dictionary<string, int> stats)
    {
        this.id = id;
        this.itemName = itemName;
        this.description = description;
        this.canEquip = canEquip;
        this.canUse = canUse;
        this.icon = Resources.Load<Sprite>("Sprites/Items/" + itemName);
        this.itemPickup = Resources.Load<GameObject>("Prefabs/ItemPickup/" + itemName);
        this.stats = stats;
    }

    public Item(Item item)
    {
        this.id = item.id;
        this.itemName = item.itemName;
        this.description = item.description;
        this.canEquip = item.canEquip;
        this.canUse = item.canUse;
        this.icon = Resources.Load<Sprite>("Sprites/Items/" + item.itemName);
        this.itemPickup = Resources.Load<GameObject>("Prefabs/ItemPickup/" + item.itemName);
        this.stats = item.stats;
    }
}
