﻿using UnityEngine;

public class ItemMenu : MonoBehaviour
{
    Item selectedItem;
    Inventory inventory;
    PlayerController player;

    // Start is called before the first frame update
    void Start()
    {
        inventory = GameObject.Find("FirstPersonPlayer").GetComponent<Inventory>();

        player = GameObject.Find("FirstPersonPlayer").GetComponent<PlayerController>();

        // Hides the item menu
        gameObject.SetActive(false);
    }

    public void ShowMenu(Item item)
    {
        // Shows the item menu
        gameObject.SetActive(true);

        // Set the position of the game object to mouse position 
        transform.position = Input.mousePosition;

        selectedItem = item;
    }

    public void HideMenu()
    {
        // Hides the item menu
        gameObject.SetActive(false);

        selectedItem = null;
    }

    public void UseItem()
    {
        switch (selectedItem.id)
        {
            case 0:
                if (PlayerVitals.instance.health < PlayerVitals.instance.maxHealth)
                {
                    float health = Mathf.Clamp(PlayerVitals.instance.health += 25, 0, PlayerVitals.instance.maxHealth);
                    PlayerVitals.instance.health = health;
                    inventory.RemoveItem(selectedItem.id);
                    HideMenu();
                }
                break;
            case 1:
                if (PlayerVitals.instance.hunger < PlayerVitals.instance.maxHunger)
                {
                    float hunger = Mathf.Clamp(PlayerVitals.instance.hunger += 10, 0, PlayerVitals.instance.maxHunger);
                    PlayerVitals.instance.hunger = hunger;
                    inventory.RemoveItem(selectedItem.id);
                    HideMenu();
                }
                break;
            case 2:
                if (PlayerVitals.instance.hunger < PlayerVitals.instance.maxHunger)
                {
                    float hunger = Mathf.Clamp(PlayerVitals.instance.hunger += 25, 0, PlayerVitals.instance.maxHunger);
                    PlayerVitals.instance.hunger = hunger;
                    inventory.RemoveItem(selectedItem.id);
                    HideMenu();
                }
                break;
            default:
                Debug.Log("No item has been selected");
                break;
        }
    }

    public void DropItem()
    {
        Debug.Log("Dropped item: " + selectedItem.itemPickup.name);

        Vector3 dropPos = new Vector3(player.transform.position.x + 2f, player.transform.forward.y + 5f, player.transform.position.z + Random.Range(-2f, 2f)); ;

        Instantiate(selectedItem.itemPickup, dropPos, player.transform.rotation);

        inventory.RemoveItem(selectedItem.id);

        HideMenu();
    }
}
