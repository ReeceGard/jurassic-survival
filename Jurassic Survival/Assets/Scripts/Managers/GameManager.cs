﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Makes a instance for other scripts to access
    public static GameManager instance { set; get; }

    // Awake is called when the object is initialised
    private void Awake()
    {
        // Tells the game to not destroy this game object
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        // Checks if the instance of the script is null
        if (instance == null)
        {
            // Sets instance to the current script
            instance = this;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            Application.Quit();
        }
    }

    public void GameOver()
    {

        Time.timeScale = 0;

        // Locks the mouse cursor and hides the cursor
        Cursor.lockState = CursorLockMode.Confined;

        Cursor.visible = true;

        UIManager.instance.gameOverScreen.SetActive(true);
    }

    public void WinGame()
    {
        Time.timeScale = 0;

        // Locks the mouse cursor and hides the cursor
        Cursor.lockState = CursorLockMode.Confined;

        Cursor.visible = true;

        UIManager.instance.winScreen.SetActive(true);
    }
}
