﻿using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public GameObject howToPlayScreen;

    public void PlayGame(string level)
    {

        // Loads into the main level
        SceneLoader.instance.LoadLevel(level);
    }

    public void OpenAboutScreen()
    {
        howToPlayScreen.SetActive(true);
    }

    public void CloseAboutScreen()
    {
        howToPlayScreen.SetActive(false);
    }
}
