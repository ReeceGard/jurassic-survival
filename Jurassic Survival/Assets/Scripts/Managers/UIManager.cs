﻿using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    // Makes a instance for other scripts to access
    public static UIManager instance { set; get; }

    public GameObject gameOverScreen;
    public GameObject winScreen;
    public Text ammoCountText;

    public GameObject pauseMenu;
    bool isGamePaused;
    AudioSource audioSource;
    PlayerControls playerControls;

    // Awake is called when the object is initialised
    void Awake()
    {
        playerControls = new PlayerControls();

        playerControls.Gameplay.Pause.performed += ctx => PauseGame();

        // Gets the UI Audio Source
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        // Checks if the instance of the script is null
        if (instance == null)
        {
            // Sets instance to the current script
            instance = this;
        }
    }

    public void RetryLevel(string level)
    {
        audioSource.Play();
        SceneLoader.instance.LoadLevel(level);
        Time.timeScale = 1;
    }

    public void MainMenu(string level)
    {
        audioSource.Play();
        SceneLoader.instance.LoadLevel(level);
        Time.timeScale = 1;
    }

    public void PauseGame()
    {

        isGamePaused = !isGamePaused;

        if (isGamePaused)
        {
            Time.timeScale = 0;

            pauseMenu.SetActive(true);

            // Locks the mouse cursor and hides the cursor
            Cursor.lockState = CursorLockMode.Confined;

            Cursor.visible = true;
        }
        else
        {
            Time.timeScale = 1;

            pauseMenu.SetActive(false);

            // Locks the mouse cursor and hides the cursor
            Cursor.lockState = CursorLockMode.Locked;

            Cursor.visible = false;
        }
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;

        pauseMenu.SetActive(false);

        isGamePaused = false;

        // Locks the mouse cursor and hides the cursor
        Cursor.lockState = CursorLockMode.Locked;
    }

    // This function is called when the game object is enabled
    void OnEnable()
    {
        // Enables the player's input when the game object is enabled
        playerControls.Gameplay.Enable();
    }

    void OnDisable()
    {
        // Disables the player's input when the game object is disabled
        playerControls.Gameplay.Disable();
    }
}
