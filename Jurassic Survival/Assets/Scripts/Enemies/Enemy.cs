﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    [Header("Setup Settings")]

    [Tooltip("The attack point")]
    public Transform attackPoint;
    NavMeshAgent agent;
    Transform player;
    Animator animator;
    AudioSource audioSource;

    [Tooltip("The layer of the ground")]
    public LayerMask groundMask;

    [Tooltip("The layer of the player")]
    public LayerMask playerMask;

    [Header("Audio Settings")]

    [Tooltip("Sound clip for when the enemy gets hurt")]
    public AudioClip hurtSound;

    [Header("Health Settings")]

    [Tooltip("The current health of the enemy")]
    public float health = 100;

    [Tooltip("The max health of the player")]
    public float maxHealth = 100;

    [Tooltip("The amount of damage the enemy can deal")]
    public float damage = 10;

    [Tooltip("The UI element that will be used as the health bar")]
    public Slider healthBar;

    public GameObject itemDrop;

    [Header("Patrolling Settings")]
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;
    float speed;
    Vector3 lastPosition;

    [Header("Attacking Settings")]
    public float timeBetweenAttacks;
    bool alreadyAttacked;
    Vector3 lookAtPlayer;
    public float turnSpeed = 5.0f;
    public Vector3 direction;

    [Header("States Settings")]
    public float sightRange;
    public float attackRange;
    bool playerInSightRange;
    bool playerInAttackRange;

    // Awake is called when the object is initialised
    private void Awake()
    {
        // Finds the player game object and gets its transform
        player = GameObject.Find("FirstPersonPlayer").transform;

        // Gets the enemy's NavMeshAgent component
        agent = GetComponent<NavMeshAgent>();

        // Gets the enemy's Audio Source
        audioSource = GetComponent<AudioSource>();

        // Gets the enemy's animator
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    private void Update()
    {
        // Check sphere for the sight and attack range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, playerMask);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, playerMask);

        /* If the player is not in the sight range and not in the attack range 
           the enemy will return to their patrol */
        if (!playerInSightRange && !playerInAttackRange)
        {
            // Tells the enemy to patrol around the map
            Patrolling();
        }

        /* If the player is in the sight range and not in the attack range 
           the enemy will chase the player */
        if (playerInSightRange && !playerInAttackRange)
        {
            // Tells the enemy to chase the player
            ChasePlayer();
        }

        /* If the player is not in the sight range and is in the attack range 
           the enemy will attack the player */
        if (playerInSightRange && playerInAttackRange)
        {
            // Tells the enemy to attack the player
            AttackPlayer();
        }

        // Updates the health bar
        healthBar.value = health / maxHealth;

        healthBar.transform.LookAt(player);

        animator.SetFloat("Speed", agent.velocity.magnitude);

        // If the enemy's health is 0 then kill the enemy
        if (health <= 0)
        {
            // Calls the function to kill the enemy
            KillEnemy();
        }     
    }

    // Tells the enemy to patrol around the map
    private void Patrolling()
    {
        // Sets the isAttacking boolean to false in the animtor
        animator.SetBool("isAttacking", false);

        // If the walk point is not set
        if (!walkPointSet)
        {
            // Searches for a walk point to move to
            SearchWalkPoint();
        }
        else
        {
            // Tells the enemy to move to the walk point
            agent.SetDestination(walkPoint);
        }

        // Calculates the distance to see the if the enemy is close to the walk point
        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        // Walk point reached
        if (distanceToWalkPoint.magnitude < 1f)
        {
            // Sets the walk point to false
            walkPointSet = false;
        }
    }

    // Searches for a walk point to move to
    private void SearchWalkPoint()
    {
        // Calculates random point on the X and Y axis in a range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        // Calculates the walk point based pn the enemy current position
        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        // Checks to see if the player is on the ground
        if (Physics.Raycast(walkPoint, -transform.up, 2f, groundMask))
        {
            // Sets the walk point to true
            walkPointSet = true;
        }
    }

    // Tells the enemy to chase the player
    private void ChasePlayer()
    {
        // Calls the function set destination as the player position
        agent.SetDestination(player.position);

        // Sets the isAttacking boolean to false in the animtor
        animator.SetBool("isAttacking", false);
    }

    // Tells the enemy to attack the player
    private void AttackPlayer()
    {
        // gets the player's direction from the enemy
        direction = player.position - transform.position;

        // Normalizes the enemy's direction to the player
        direction.Normalize();

        // Sets the enemy's rotaion to face the player
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), turnSpeed * Time.deltaTime);

        // Set the enemy's rotaion on the Y axis to 90
        transform.rotation = Quaternion.Euler(0, 90, 0);

        // Stops the enemy from walking to attack the player
        agent.SetDestination(transform.position);

        // Checks to see if the enemy has not attacked
        if (!alreadyAttacked)
        {
            // Sets the isAttacking bool in the animator to true
            animator.SetBool("isAttacking", true);

            // Enemy has already attacked
            alreadyAttacked = true;
            
            // Resets the enemy's attack
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }

    // Resets the enemy's attacking
    public void ResetAttack()
    {
        // Sets the isAttacking boolean to false in the animtor
        animator.SetBool("isAttacking", false);

        // Allows the enemy to attack
        alreadyAttacked = false;
    }

    // Takes health away from the enemy when damaged
    public void TakeDamage(int damage)
    {
        // Takes away health 
        health -= damage;

        // Plays audio when the play enemy takes damage
        audioSource.Play();
    }

    // Destroys the enemy
    private void KillEnemy()
    {
        // Sets the isAttacking boolean to false in the animtor
        animator.SetBool("isAttacking", false);

        // Spawns a food item when the enemy is killed
        Instantiate(itemDrop, transform.position, transform.rotation);

        // Removes the game object from the game
        Destroy(gameObject);
    }

    // Draws debug gizmo when selected
    private void OnDrawGizmosSelected()
    {
        // Sets the attack sphere colour to red
        Gizmos.color = Color.red;

        // Draws a wire sphere to visualise the attack range
        Gizmos.DrawWireSphere(transform.position, attackRange);

        // Sets the sight sphere colour to red
        Gizmos.color = Color.yellow;

        // Draws a wire sphere to visualise the sight range
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
}
