﻿using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [Tooltip("The amount of damage the enemy can deal")]
    public float damage = 10;

    // When a game object enters the trigger
    private void OnTriggerEnter(Collider collider)
    {
        // If the collided game object has the player tag
        if (collider.tag == "Player")
        {
            // Calls the take damage function
            PlayerVitals.instance.TakeDamage(damage);
        }
    }
}
