﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Preloader : MonoBehaviour
{
    public string levelName;

    public GameObject loadingScreen;

    // Start is called before the first frame update
    void Start()
    {
        // Starts the LoadAsychronously coroutine, which will load a level asychronously
        StartCoroutine(LoadAsychronously(levelName));
    }

    // Load the level asychronously
    IEnumerator LoadAsychronously(string level)
    {
        // Calls the function to load the level asychronously
        SceneManager.LoadSceneAsync(level);

        // Shows the loading screen
        loadingScreen.SetActive(true);
        
        yield return null;
    }
}
