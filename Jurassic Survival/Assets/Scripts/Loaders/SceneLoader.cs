﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    // Makes a instance for other scripts to access
    public static SceneLoader instance { set; get; }

    // Loading screen game object
    public GameObject loadingScreen;

    // Variable array that will store UI elements
    public GameObject gameUI;

    // Start is called before the first frame update
    private void Start()
    {
        // Checks if the instance of the script is null
        if (instance == null)
        {
            // Sets instance to the current script
            instance = this;
        }
    }

    // Loads the level
    public void LoadLevel(string level)
    {
        // Starts the LoadAsychronously coroutine, which will load a level asychronously
        StartCoroutine(LoadAsychronously(level));
    }

    // Load the level asychronously
    IEnumerator LoadAsychronously(string level)
    {
        // Calls the function to load the level asychronously
        SceneManager.LoadSceneAsync(level);

            // Hides the game UI
            gameUI.SetActive(false);

        // Shows the loading screen
        loadingScreen.SetActive(true);
        
        yield return null;
    }
}
