﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerVitals : MonoBehaviour
{
    // Makes a instance for other scripts to access
    public static PlayerVitals instance { set; get; }

    [Header("Player Health Settings")]

    [Tooltip("The current health of the player")]
    public float health = 100f;

    [Tooltip("The max health of the player")]
    public float maxHealth = 100f;

    [Tooltip("The rate that the health decays at")]
    public float healthDecayRate = 5f;

    [Tooltip("The amount of health that is taken away")]
    public float healthDecayAmount = 1f;

    [Tooltip("The UI element that will be used as the health bar")]
    public Slider healthBar;


    [Header("Player Hunger Settings")]

    [Tooltip("The current hunger of the player")]
    public float hunger = 100f;

    [Tooltip("The max hunger of the player")]
    public float maxHunger = 100f;

    [Tooltip("The rate that the hunger decays at")]
    public float hungerDecayRate = 5f;

    [Tooltip("The amount of hunger that is taken away")]
    public float hungerDecayAmount = 1f;

    [Tooltip("The UI element that will be used as the hunger bar")]
    public Slider hungerBar;

    AudioSource audioSource;

    // Awake is called when the object is initialised
    void Awake()
    {
        // Checks if the instance of the script is null
        if (instance == null)
        {
            // Sets instance to the current script
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Calls the function to take hunger away and repeats it
        InvokeRepeating("TakeAwayHunger", 0, hungerDecayRate);

        // Gets the enemy's Audio Source
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        // Updates the health bar
        healthBar.value = health / maxHealth;

        // Updates the hunger bar
        hungerBar.value = hunger / maxHunger;

        //if ()

        if (health <= 0)
        {
            PlayerController.instance.KillPlayer();
        }
    }

    // Takes aways hunger points from the player
    public void TakeAwayHunger()
    {
        // Takes away hunger
        hunger -= hungerDecayAmount;

        // If hunger is below 0
        if (hunger < 0)
        {
            // Sets hunger to 0
            hunger = 0;
        }
    }

    // Player takes damage
    public void TakeDamage(float damage)
    {
        if (health > 0)
        {
            // Takes away some health
            health -= damage;

            // Plays audio when the play enemy takes damage
            audioSource.Play();
        }
    }
}
