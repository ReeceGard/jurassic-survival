﻿using UnityEngine;
using UnityEngine.UI;

public class Pistol : MonoBehaviour
{
    public static Pistol instance { set; get; }

    [Header("Gun Settings")]
    public Transform firepoint;
    public GameObject bulletPrefab;
    public float bulletForce = 100f;
    public int currentAmmo = 200;
    public int maxAmmo = 200;
    public Text ammoText;

    AudioSource audioSource;

    PlayerControls controls;

    void Awake()
    {
        controls = new PlayerControls();

        controls.Gameplay.Attack.performed += ctx => FireGun();

        audioSource = GetComponent<AudioSource>();

        ammoText = GameObject.Find("AmmoText").GetComponent<Text>();
    }

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Update()
    {
        ammoText.text = "Ammo: " + currentAmmo.ToString();
    }

    void FireGun()
    {
        if (currentAmmo >= 1 && !PlayerController.instance.isInMenu && Time.timeScale == 1)
        {
            audioSource.Play();
            GameObject bullet = Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(firepoint.forward * bulletForce, ForceMode.Impulse);
            currentAmmo -= 1;
        }
    }

    // This function is called when the game object is enabled
    void OnEnable()
    {
        // Enables the player's input when the game object is enabled
        controls.Gameplay.Enable();
    }

    // This function is called when the game object is disabled
    void OnDisable()
    {
        // Disables the player's input when the game object is disabled
        controls.Gameplay.Disable();
    }
}
