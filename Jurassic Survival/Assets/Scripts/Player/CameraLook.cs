﻿using UnityEngine;

public class CameraLook : MonoBehaviour
{
    public float mouseSensitivity = 100f;

    public Transform playerTransform;

    float xRotation = 0f;
   [SerializeField] Vector3 mouse;

    PlayerControls playerControls;

    // Awake is called when the object is initialised
    private void Awake()
    {
        playerControls = new PlayerControls();

        playerControls.Gameplay.LookUp.performed += ctx => mouse.y = ctx.ReadValue<float>();
        playerControls.Gameplay.LookRight.performed += ctx => mouse.x = ctx.ReadValue<float>();
    }

    // Start is called before the first frame update
    void Start()
    {
        // Locks the mouse cursor and hides the cursor
        Cursor.lockState = CursorLockMode.Locked;


        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!PlayerController.instance.isInMenu)
        {
            mouse.x *= mouseSensitivity * Time.deltaTime;
            mouse.y *= mouseSensitivity * Time.deltaTime;

            xRotation -= mouse.y;
            xRotation = Mathf.Clamp(xRotation, -90f, 90f);

            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            playerTransform.Rotate(Vector3.up * mouse.x);
        }
    }

    void OnEnable()
    {
        // Enables the player's input when the game object is enabled
        playerControls.Gameplay.Enable();
    }

    void OnDisable()
    {
        // Disables the player's input when the game object is disabled
        playerControls.Gameplay.Disable();
    }
}
