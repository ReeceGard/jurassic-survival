﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int damage = 5;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            GameObject enemy = collision.gameObject;
            enemy.GetComponent<Enemy>().TakeDamage(damage);

            Destroy(gameObject);
        } else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        Destroy(gameObject, 5);
    }
}
