﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerController : MonoBehaviour
{
    // Makes a instance for other scripts to access
    public static PlayerController instance { set; get; }

    [Header("Player Setup Settings")]
    public Vector3 velocity;
    public CharacterController controller;
    public Camera playerCamera;
    public Transform playerHand;

    [Header("Player Speed Settings")]
    public float walkSpeed = 3f;
    public float runSpeed = 5f;
    public float crouchSpeed = 1f;

    [Header("Inventory Settings")]
    public RectTransform inventoryUI;
    public Text interactText;
    public bool canPickup;

    [Header("Inventory Settings")]
    public bool isInMenu;

    [Header("Physics Settings")]
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public float gravity = -1.81f;
    public float jumpHeight = 12f;

    // Declaring private varibles
    Vector3 movement;
    bool isGrounded;
    float currentSpeed = 2f;
    float interactionDistance = 2f;

    ItemPickup pickup;
    PlayerControls playerControls;

    // Awake is called when the object is initialised
    private void Awake()
    {
        // Checks if the instance of the script is null
        if (instance == null)
        {
            // Sets instance to the current script
            instance = this;
        }

        // Player Inputs
        playerControls = new PlayerControls();

        playerControls.Gameplay.MoveForward.performed += ctx => movement.z = ctx.ReadValue<float>();
        playerControls.Gameplay.MoveForward.canceled += ctx => movement.z = 0;

        playerControls.Gameplay.MoveRight.performed += ctx => movement.x = ctx.ReadValue<float>();
        playerControls.Gameplay.MoveRight.canceled += ctx => movement.x = 0;

        playerControls.Gameplay.Jump.performed += ctx => Jump();

        playerControls.Gameplay.Inventory.performed += ctx => OpenInventory();

        playerControls.Gameplay.Interact.performed += ctx => Pickup();

        playerControls.Gameplay.Crouch.performed += ctx => Crouch(true);
        playerControls.Gameplay.Crouch.canceled += ctx => Crouch(false);

        playerControls.Gameplay.Run.performed += ctx => Run(true);
        playerControls.Gameplay.Run.canceled += ctx => Run(false);

        interactText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        canPickup = false;
        interactText.text = "";

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        if (!isInMenu)
        {
            Vector3 move = transform.right * movement.x + transform.forward * movement.z;

            controller.Move(move * currentSpeed * Time.deltaTime);

            velocity.y += gravity * Time.deltaTime;

            velocity = move * currentSpeed + Vector3.up * velocity.y;

            controller.Move(velocity * Time.deltaTime);
        }

        RaycastHit hit;

        if (Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, interactionDistance))
        {
            ItemPickup itemPickup = hit.collider.GetComponent<ItemPickup>();

            pickup = itemPickup;

            if (itemPickup != null)
            {
                canPickup = true;
                interactText.text = "Press 'E' To Pick Up " + pickup.itemName;
            } 
            else
            {
                canPickup = false;
                interactText.text = "";
            }
        }
    }

    // Makes the player
    public void Jump()
    {
        // Checks to see if the player is on the ground
        if (isGrounded && !isInMenu)
        {
            // Launches the player in the air
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }
    }

    // Makes the player run
    public void Run(bool isRunning)
    {
        // If the player is running
        if (isRunning)
        {
            // Sets the speed of the player to the run speed
            currentSpeed = runSpeed;
        } else
        {
            // Sets the speed of the player to the walk speed
            currentSpeed = walkSpeed;
        }
    }

    // Makes the player crouch and uncrouch
    public void Crouch(bool isCrouching)
    {
        if (isCrouching && !isInMenu)
        {
            // Changes the controller height to the crouched height
            controller.height = 1f;
            
            // Changes the current speed to the crouch speed
            currentSpeed = crouchSpeed;
        } 
        else
        {
            // Changes the controller height to the full height of the player
            controller.height = 1.8f;

            // Changes the current speed to the walk speed
            currentSpeed = walkSpeed;
        }
    }

    // Kills the player
    public void KillPlayer()
    {
        // Ends the game and displays the game over screen
        GameManager.instance.GameOver();
    }

    // Open and close the inventory
    public void OpenInventory()
    {
        // Sets the isInInventory to true or false
        isInMenu = !isInMenu;

        // Checks to see if the player is in the inventory
        if (isInMenu)
        {
            // Moves the inventory screen to the center of the screen
            inventoryUI.anchoredPosition = Vector3.zero;

            // Unlocks the mouse cursor
            Cursor.lockState = CursorLockMode.Confined;

            // Unhides the mouse cursor
            Cursor.visible = true;
        }
        else
        {
            // Moves the inventory screen off the screen
            inventoryUI.anchoredPosition = new Vector3(-960, 960, 0);

            // Unlocks the mouse cursor
            Cursor.lockState = CursorLockMode.Locked;

            // Unhides the mouse cursor
            Cursor.visible = false;
        }
    }

    // Pickups the item
    public void Pickup()
    {
        // If the player can pickup the item
        if (canPickup)
        {
            // Call the function to pickup the item
            pickup.GetComponent<ItemPickup>().PickupItem();
        }
    }

    // This function is called when the game object is enabled
    void OnEnable()
    {
        // Enables the player's input when the game object is enabled
        playerControls.Gameplay.Enable();
    }

    // This function is called when the game object is disabled
    void OnDisable()
    {
        // Disables the player's input when the game object is disabled
        playerControls.Gameplay.Disable();
    }
}
